
function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

async function pullItems(ls, selector) {
	for (e of Array.from(document.querySelectorAll(selector)).map(x => x.href)) {
		await sleep(100); 
		fetch(e).then(x => x.text()).then(x => ls.push(parser.parseFromString(x, "text/html")));
	}
}

async function openFiles(ls) {
	for (e of ls.map(x => x.querySelector("#content a")).map(x => x == null || x.href == null ? null : x.href)) {
		await sleep(2000);
		window.open(e);
	}
}

function downloadFiles() {
	files = [];
	pullItems(files, ".type_icon[title=Attachment]+div a");
	openFiles(files);
}


let parser = new DOMParser();
let readResponse = text => JSON.parse(text.replace("while(1);", ""));

// Example apiHead: https://canvas.brown.edu/api/v1/
async function download(apiHead, options = {
	includeDiscussions: true,
	includeAssignments: true,
	includePages: true,
	includeModules: true
}) {

	await fetch(apiHead + "courses?per_page=100").then(x => x.text()).then(async text => {

		let courses = readResponse(text);
		let data = {}
		data.courses = [];

		for (let e of courses) {
			let course = {};
			Object.assign(course, e);	
			courseId = e.id;
			
			if (options.includeDiscussions) {
				course.discussions = []
				await fetch(apiHead + "courses/" + courseId + "/discussion_topics?per_page=100").then(x => x.text()).then(async text => {
					let discussionTopics;
					try { discussionTopics = readResponse(text); } catch (e) { return }
					for (let discussion of discussionTopics) {
						try {
							await fetch(apiHead + "courses/" + courseId + "/discussion_topics/" + discussion.id + "/view").then(x=>x.text()).then(text=>{
								Object.assign(discussion, readResponse(text));	
								course.discussions.push(discussion);
							});
						} catch (e) {
							continue;
						}
					}
				})
			}
		
			if (options.includeAssignments) {
				course.assignments = [];
				await fetch(apiHead + "courses/" + courseId + "/assignments?per_page=100").then(x => x.text()).then(async text => {
					let assignments = readResponse(text);
					course.assignments = assignments;
				})
			}
	
			if (options.includePages) {
				course.pages = [];
				await fetch(apiHead + "courses/" + courseId + "/pages?per_page=100").then(x => x.text()).then(async text => {
					let pages = readResponse(text) || [];
					if (pages != null && pages.length > 0) {
						for (let page of pages) {
							await fetch(page.html_url).then(response => { 
								response.text().then(text => {
									let html = parser.parseFromString(text, "text/html");
									page.content = html.body.innerHTML;
								});
							})
						}
					}
					course.pages = pages;
				})
			}

			if (options.includeModules) {
				course.modules = [];
				await fetch(apiHead + "courses/" + courseId + "/modules?per_page=100").then(x => x.text()).then(async text => {
					let modules = readResponse(text);
					course.modules = modules;
				});
			}

			data.courses.push(course);
		};
		let dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(data));
		let dlAnchorElem = document.createElement("a");
		dlAnchorElem.setAttribute("href", dataStr);
		dlAnchorElem.setAttribute("download", courseId + ".json");
		dlAnchorElem.click();
	});
}
